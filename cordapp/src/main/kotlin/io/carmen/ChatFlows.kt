package io.carmen

import co.paralleluniverse.fibers.Suspendable
import io.carmen.chat.Chat
import net.corda.core.contracts.UniqueIdentifier
import net.corda.core.flows.FinalityFlow
import net.corda.core.flows.FlowLogic
import net.corda.core.flows.InitiatingFlow
import net.corda.core.flows.StartableByRPC
import net.corda.core.identity.Party
import net.corda.core.transactions.SignedTransaction
import net.corda.core.transactions.TransactionBuilder
import net.corda.core.utilities.ProgressTracker
import java.time.Instant.now
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter
import java.time.format.FormatStyle

// *********
// * Flows *
// *********

@InitiatingFlow
@StartableByRPC
class SendChat(private val to: Party, private val message: String) : FlowLogic<Unit>() {
    override val progressTracker = ProgressTracker(object : ProgressTracker.Step("Sending") {})

    @Suspendable
    override fun call() {
        val stx: SignedTransaction = createMessageStx()
        progressTracker.nextStep()
        subFlow(FinalityFlow(stx))
    }

    private fun createMessageStx(): SignedTransaction {
        val notary = serviceHub.networkMapCache.notaryIdentities.first()
        val txb = TransactionBuilder(notary)
        val me = ourIdentityAndCert.party
        val sent = true
        val delivered = false
        val fromMe = true
        val time = LocalDateTime.now()
        val formatter = DateTimeFormatter.ofLocalizedDateTime(FormatStyle.MEDIUM)
        val formatted = time.format(formatter)
        txb.addOutputState(Chat.Message(UniqueIdentifier(), message, to, me, sent, delivered, fromMe, formatted), Chat::class.qualifiedName!!)
        txb.addCommand(Chat.SendChatCommand, me.owningKey)
        return serviceHub.signInitialTransaction(txb)
    }

}




@InitiatingFlow
@StartableByRPC
class SendFile(private val to: Party, private val attachment: String) : FlowLogic<Unit>() {
    override val progressTracker = ProgressTracker(object : ProgressTracker.Step("Sending") {})

    @Suspendable
    override fun call() {
        val stx: SignedTransaction = createAttachmentStx()
        progressTracker.nextStep()
        subFlow(FinalityFlow(stx))
    }

    private fun createAttachmentStx(): SignedTransaction {
        val notary = serviceHub.networkMapCache.notaryIdentities.first()
        val txb = TransactionBuilder(notary)
        val me = ourIdentityAndCert.party
        val sent = true
        val delivered = false
        val fromMe = true
        val time = LocalDateTime.now()
        val formatter = DateTimeFormatter.ofLocalizedDateTime(FormatStyle.MEDIUM)
        val formatted = time.format(formatter)
        txb.addOutputState(Chat.Attachment(UniqueIdentifier(), attachment, to, me, sent, delivered, fromMe, formatted), Chat::class.qualifiedName!!)
        txb.addCommand(Chat.SendFileCommand, me.owningKey)
        return serviceHub.signInitialTransaction(txb)
    }

}

