package io.carmen.webserver

import io.carmen.*
import io.carmen.account.Account
import io.carmen.account.AccountSchemaV1
import io.carmen.case.Case
import io.carmen.case.CaseSchemaV1
import io.carmen.chat.Chat
import io.carmen.contact.Contact
import io.carmen.contact.ContactSchemaV1
import io.carmen.lead.Lead
import io.carmen.lead.LeadSchemaV1
import net.corda.core.contracts.ContractState
import net.corda.core.contracts.LinearState
import net.corda.core.contracts.StateAndRef
import net.corda.core.contracts.UniqueIdentifier
import net.corda.core.identity.CordaX500Name
import net.corda.core.messaging.vaultQueryBy
import net.corda.core.node.ServiceHub
import net.corda.core.node.services.Vault
import net.corda.core.node.services.queryBy
import net.corda.core.node.services.vault.*
import net.corda.core.node.services.vault.Builder.equal
import net.corda.core.utilities.getOrThrow
import org.bouncycastle.asn1.x509.X509ObjectIdentifiers.commonName
import org.slf4j.LoggerFactory
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import org.springframework.web.bind.annotation.RestController
import sun.security.timestamp.TSResponse
import java.time.LocalDateTime
import java.time.ZoneId


/**
 * Define your API endpoints here.
 */


@RestController
@RequestMapping("/api") // The paths for HTTP requests are relative to this base path.
class RestController(
        private val rpc: NodeRPCConnection) {


    companion object {
        private val logger = LoggerFactory.getLogger(RestController::class.java)
    }


    private val me = rpc.proxy.nodeInfo().legalIdentities.first().name

    private inline fun <reified U : ContractState> getState(
            services: ServiceHub,
            block: (generalCriteria: QueryCriteria.VaultQueryCriteria) -> QueryCriteria
    ): List<StateAndRef<U>> {
        val query = builder {
            val generalCriteria = QueryCriteria.VaultQueryCriteria(Vault.StateStatus.UNCONSUMED)
            block(generalCriteria)
        }
        val result = services.vaultService.queryBy<U>(query)
        return result.states
    }


    /** Maps an Account to a JSON object. */

    private fun Account.toJson(): Map<String, String> {
        return kotlin.collections.mapOf(
                "accountId" to accountId,
                "accountName" to accountName,
                "accountType" to accountType,
                "industry" to industry,
                "phone" to phone,
                "controller" to controller.name.organisation,
                "processor" to processor.name.organisation,
                "linearId" to linearId.toString())
    }


    /** Maps an Contact to a JSON object. */

    private fun Contact.toJson(): Map<String, String> {
        return kotlin.collections.mapOf(
                "contactId" to contactId,
                "firstName" to firstName,
                "lastName" to lastName,
                "email" to email,
                "phone" to phone,
                "controller" to controller.name.organisation,
                "processor" to processor.name.organisation,
                "linearId" to linearId.toString())
    }


    /** Maps an Lead to a JSON object. */


    private fun Lead.toJson(): Map<String, String> {
        return kotlin.collections.mapOf(
                "leadId" to leadId,
                "firstName" to firstName,
                "lastName" to lastName,
                "email" to email,
                "phone" to phone,
                "controller" to controller.name.organisation,
                "processor" to processor.name.organisation,
                "linearId" to linearId.toString())
    }


    /** Maps an Case to a JSON object. */

    private fun Case.toJson(): Map<String, String> {
        return kotlin.collections.mapOf(
                "caseId" to caseId,
                "description" to description,
                "caseNumber" to caseNumber,
                "caseStatus" to caseStatus.toString(),
                "priority" to casePriority.toString(),
                "submitter" to submitter.toString(),
                "resolver" to resolver.toString())
    }


    /** Maps an Chat to a JSON object. */

    private fun Chat.Message.toJson(): Map<String, String> {
        return kotlin.collections.mapOf(
                "messageId" to messageId.toString(),
                "message" to message,
                "to" to to.name.organisation,
                "from" to from.name.organisation,
                "sentReceipt" to sentReceipt.toString(),
                "deliveredReceipt" to deliveredReceipt.toString(),
                "fromMe" to fromMe.toString(),
                "time" to time.toString())
    }



    /** Returns the node's name. */
    @GetMapping(value = "/me", produces = arrayOf("text/plain"))
    private fun me() = me.toString()

    @GetMapping(value = "/status", produces = arrayOf("text/plain"))
    private fun status() = "200"

    @GetMapping(value = "/servertime", produces = arrayOf("text/plain"))
    private fun serverTime() = LocalDateTime.ofInstant(proxy.currentNodeTime(), ZoneId.of("UTC")).toString()

    @GetMapping(value = "/addresses", produces = arrayOf("text/plain"))
    private fun addresses() = proxy.nodeInfo().addresses.toString()

    @GetMapping(value = "/identities", produces = arrayOf("text/plain"))
    private fun identities() = proxy.nodeInfo().legalIdentities.toString()

    @GetMapping(value = "/platformversion", produces = arrayOf("text/plain"))
    private fun platformVersion() = proxy.nodeInfo().platformVersion.toString()

    @GetMapping(value = "/peers", produces = arrayOf("text/plain"))
    private fun peers() = proxy.networkMapSnapshot().flatMap { it.legalIdentities }.toString()

    @GetMapping(value = "/notaries", produces = arrayOf("text/plain"))
    private fun notaries() = proxy.notaryIdentities().toString()

    @GetMapping(value = "/flows", produces = arrayOf("text/plain"))
    private fun flows() = proxy.registeredFlows().toString()


    private val proxy = rpc.proxy


    /** Returns the previous consumed Agreement State */

    @GetMapping(value = "/getPreviousState", produces = arrayOf("application/json"))
    fun getPreviousStateByLinearId(@PathVariable linearId: String, services: ServiceHub): StateAndRef<LinearState>? {
        val agreementId = UniqueIdentifier(linearId)
        val pagingSpec = PageSpecification(DEFAULT_PAGE_NUM, 10)
        val sortAttribute = SortAttribute.Standard(Sort.LinearStateAttribute.UUID)
        val linearStateCriteria = QueryCriteria.LinearStateQueryCriteria(linearId = listOf(agreementId), status = Vault.StateStatus.CONSUMED)
        val vaultCriteria = QueryCriteria.VaultQueryCriteria(status = Vault.StateStatus.CONSUMED)
        val results = proxy.vaultQueryBy<LinearState>(linearStateCriteria and vaultCriteria, pagingSpec, Sort(setOf(Sort.SortColumn(sortAttribute, Sort.Direction.DESC))))

        return results.states.singleOrNull()

    }


    /** Returns the next consumed Agreement State */

    @GetMapping(value = "/getNextState", produces = arrayOf("application/json"))
    fun getNextStateByLinearId(@PathVariable linearId: String, services: ServiceHub): StateAndRef<LinearState>? {
        val agreementId = UniqueIdentifier(linearId)
        val pagingSpec = PageSpecification(DEFAULT_PAGE_NUM, 1)
        val sortAttribute = SortAttribute.Standard(Sort.LinearStateAttribute.UUID)
        val linearStateCriteria = QueryCriteria.LinearStateQueryCriteria(linearId = listOf(agreementId), status = Vault.StateStatus.CONSUMED)
        val vaultCriteria = QueryCriteria.VaultQueryCriteria(status = Vault.StateStatus.CONSUMED)
        val results = proxy.vaultQueryBy<LinearState>(linearStateCriteria and vaultCriteria, pagingSpec, Sort(setOf(Sort.SortColumn(sortAttribute, Sort.Direction.ASC))))

        return results.states.singleOrNull()

    }

    /** Returns a list of existing Accounts. */

    @GetMapping(value = "/getAccounts", produces = arrayOf("application/json"))
    fun getAccounts(): List<Map<String, String>> {
        val accountStateAndRefs = rpc.proxy.vaultQueryBy<Account>().states
        val accountStates = accountStateAndRefs.map { it.state.data }
        return accountStates.map { it.toJson() }
    }


    /** Returns an Account by the Account Id */


    @GetMapping(value = "/getAccounts/{accountId}", produces = arrayOf("application/json"))
    fun getAccountByAccountId(@PathVariable accountId: String, services: ServiceHub): StateAndRef<Account>? {
        val states = getState<Account>(services) { generalCriteria ->
            val additionalCriteria = QueryCriteria.VaultCustomQueryCriteria(AccountSchemaV1.PersistentAccount::accountId.equal(accountId))
            generalCriteria.and(additionalCriteria)
        }
        return states.singleOrNull()
    }


    /** Returns a list of existing Agreements. */


    @GetMapping(value = "/getContacts", produces = arrayOf("application/json"))
    fun getContacts(): List<Map<String, String>> {
        val contactStateAndRefs = rpc.proxy.vaultQueryBy<Contact>().states
        val contactStates = contactStateAndRefs.map { it.state.data }
        return contactStates.map { it.toJson() }
    }

    /** Returns a Contact by the Contact Id */


    @GetMapping(value = "/getContact/{contactId}", produces = arrayOf("application/json"))
    fun getContactByContactId(@PathVariable contactId: String, services: ServiceHub): StateAndRef<Contact>? {
        val states = getState<Contact>(services) { generalCriteria ->
            val additionalCriteria = QueryCriteria.VaultCustomQueryCriteria(ContactSchemaV1.PersistentContact::contactId.equal(contactId))
            generalCriteria.and(additionalCriteria)
        }
        return states.singleOrNull()
    }



    /** Returns a list of existing Leads. */


    @GetMapping(value = "/getLeads", produces = arrayOf("application/json"))
    fun getLeads(): List<Map<String, String>> {
        val leadStateAndRefs = rpc.proxy.vaultQueryBy<Lead>().states
        val leadStates = leadStateAndRefs.map { it.state.data }
        return leadStates.map { it.toJson() }
    }


    /** Returns a Lead by the Lead Id */


    @GetMapping(value = "/getLead/{leadId}", produces = arrayOf("application/json"))
    fun getLeadByLeadId(@PathVariable leadId: String, services: ServiceHub): StateAndRef<Lead>? {
        val states = getState<Lead>(services) { generalCriteria ->
            val additionalCriteria = QueryCriteria.VaultCustomQueryCriteria(LeadSchemaV1.PersistentLead::leadId.equal(leadId))
            generalCriteria.and(additionalCriteria)
        }
        return states.singleOrNull()
    }

    /** Returns a list of existing Cases. */


    @GetMapping(value = "/getCases", produces = arrayOf("application/json"))
    fun getCases(): List<Map<String, String>> {
        val caseStateAndRefs = rpc.proxy.vaultQueryBy<Case>().states
        val caseStates = caseStateAndRefs.map { it.state.data }
        return caseStates.map { it.toJson() }
    }



    /** Returns a Contact by the Contact Id */


    @GetMapping(value = "/getCase/{caseid}", produces = arrayOf("application/json"))
    fun getCaseByCaseId(@PathVariable caseId: String, services: ServiceHub): StateAndRef<Case>? {
        val states = getState<Case>(services) { generalCriteria ->
            val additionalCriteria = QueryCriteria.VaultCustomQueryCriteria(CaseSchemaV1.PersistentCase::caseId.equal(caseId.toString()))
            generalCriteria.and(additionalCriteria)
        }
        return states.singleOrNull()
    }




    /** Returns a list of existing Messages. */


    @GetMapping(value = "/getMessages", produces = arrayOf("application/json"))
    fun getMessages(): List<Map<String, String>> {
        val messageStateAndRefs = rpc.proxy.vaultQueryBy<Chat.Message>().states
        val messageStates = messageStateAndRefs.map { it.state.data }
        return messageStates.map { it.toJson() }
    }



    /** Creates an Agreement. */

    @PostMapping(value = "/createAccount")
    fun createAccount(@RequestParam("accountId") accountId: String,
                      @RequestParam("accountName") accountName: String,
                      @RequestParam("accountType") accountType: String,
                      @RequestParam("industry") industry: String,
                      @RequestParam("phone") phone: String,
                      @RequestParam("processor") processor: String?): ResponseEntity<Any?> {


        if (processor == null) {
            return ResponseEntity.status(TSResponse.BAD_REQUEST).body("Query parameter 'counterPartyName' missing or has wrong format.\n")
        }

        val counterparty = CordaX500Name.parse(processor)

        val otherParty = proxy.wellKnownPartyFromX500Name(counterparty)
                ?: return ResponseEntity.status(TSResponse.BAD_REQUEST).body("Party named $processor cannot be found.\n")

        val (status, message) = try {

            val flowHandle = proxy.startFlowDynamic(CreateAccountFlow.Controller::class.java, accountId, accountName, accountType, industry, phone, otherParty)

            val result = flowHandle.use { it.returnValue.getOrThrow() }

            HttpStatus.CREATED to "Transaction id ${result.tx.id} committed to ledger.\n${result.tx.outputs.single().data}"

        } catch (e: Exception) {
            HttpStatus.BAD_REQUEST to e.message
        }
        logger.info(message)
        return ResponseEntity<Any?>(message, status)
    }


    /** Creates a Contact. */

    @PostMapping(value = "/createContact")
    fun createContact(@RequestParam("contactId") contactId: String,
                      @RequestParam("firstName") firstName: String,
                      @RequestParam("lastName") lastName: String,
                      @RequestParam("email") email: String,
                      @RequestParam("phone") phone: String,
                      @RequestParam("processor") processor: String?): ResponseEntity<Any?> {


        if (processor == null) {
            return ResponseEntity.status(TSResponse.BAD_REQUEST).body("Query parameter 'counterPartyName' missing or has wrong format.\n")
        }

        val counterparty = CordaX500Name.parse(processor)

        val otherParty = proxy.wellKnownPartyFromX500Name(counterparty)
                ?: return ResponseEntity.status(TSResponse.BAD_REQUEST).body("Party named $processor cannot be found.\n")

        val (status, message) = try {

            val flowHandle = proxy.startFlowDynamic(CreateContactFlow.Controller::class.java, contactId, firstName, lastName, email, phone, otherParty)

            val result = flowHandle.use { it.returnValue.getOrThrow() }

            HttpStatus.CREATED to "Transaction id ${result.tx.id} committed to ledger.\n${result.tx.outputs.single().data}"

        } catch (e: Exception) {
            HttpStatus.BAD_REQUEST to e.message
        }
        logger.info(message)
        return ResponseEntity<Any?>(message, status)
    }


    /** Creates a Lead. */



    @PostMapping(value = "/createLead")
    fun createLead(@RequestParam("leadId") leadId: String,
                   @RequestParam("firstName") firstName: String,
                   @RequestParam("lastName") lastName: String,
                   @RequestParam("company") company: String,
                   @RequestParam("title") title: String,
                   @RequestParam("email") email: String,
                   @RequestParam("phone") phone: String,
                   @RequestParam("country") country: String,
                   @RequestParam("processor") processor: String?): ResponseEntity<Any?> {


        if (processor == null) {
            return ResponseEntity.status(TSResponse.BAD_REQUEST).body("Query parameter 'counterPartyName' missing or has wrong format.\n")
        }

        val counterparty = CordaX500Name.parse(processor)

        val otherParty = proxy.wellKnownPartyFromX500Name(counterparty)
                ?: return ResponseEntity.status(TSResponse.BAD_REQUEST).body("Party named $processor cannot be found.\n")

        val (status, message) = try {

            val flowHandle = proxy.startFlowDynamic(CreateLeadFlow.Controller::class.java, leadId, firstName, lastName, company, title, email, phone, country, otherParty)

            val result = flowHandle.use { it.returnValue.getOrThrow() }

            HttpStatus.CREATED to "Transaction id ${result.tx.id} committed to ledger.\n${result.tx.outputs.single().data}"

        } catch (e: Exception) {
            HttpStatus.BAD_REQUEST to e.message
        }
        logger.info(message)
        return ResponseEntity<Any?>(message, status)
    }


    /** Creates a Case. */

    @PostMapping(value = "/createCase")
    fun createCase(@RequestParam("caseId") caseId: String,
                   @RequestParam("description") description: String,
                   @RequestParam("caseNumber") caseNumber: String,
                   @RequestParam("casePriority") casePriority: String,
                   @RequestParam("resolver") resolver: String?): ResponseEntity<Any?> {


        if (resolver == null) {
            return ResponseEntity.status(TSResponse.BAD_REQUEST).body("Query parameter 'counterPartyName' missing or has wrong format.\n")
        }

        val counterparty = CordaX500Name.parse(resolver)

        val otherParty = proxy.wellKnownPartyFromX500Name(counterparty)
                ?: return ResponseEntity.status(TSResponse.BAD_REQUEST).body("Party named $resolver cannot be found.\n")

        val (status, message) = try {

            val flowHandle = proxy.startFlowDynamic(CreateCaseFlow.Initiator::class.java, caseId, description, caseNumber, casePriority, otherParty)

            val result = flowHandle.use { it.returnValue.getOrThrow() }

            HttpStatus.CREATED to "Transaction id ${result.tx.id} committed to ledger.\n${result.tx.outputs.single().data}"

        } catch (e: Exception) {
            HttpStatus.BAD_REQUEST to e.message
        }
        logger.info(message)
        return ResponseEntity<Any?>(message, status)
    }


    /** Send Chat */



    @PostMapping(value = "/sendChat")
    fun sendChat(@RequestParam("to") to: String,
                 @RequestParam("message") message: String): ResponseEntity<Any?> {

        if (message == null) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Query parameter 'message' can not be null.\n")
        }

        if (to == null) {
            return ResponseEntity.status(TSResponse.BAD_REQUEST).body("Query parameter 'recipient' missing or has wrong format.\n")
        }

        val counterparty = CordaX500Name.parse(to)


        val to = proxy.wellKnownPartyFromX500Name(counterparty)
                ?: return ResponseEntity.status(TSResponse.BAD_REQUEST).body("Party named $to cannot be found.\n")

        val (status, message) = try {


            val flowHandle = proxy.startFlowDynamic(SendChat::class.java, to, message)

            val result = flowHandle.use { it.returnValue.getOrThrow() }

            HttpStatus.CREATED to "Message sent."

        } catch (e: Exception) {
            HttpStatus.BAD_REQUEST to e.message
        }
        logger.info(message)
        return ResponseEntity<Any?>(message, status)
    }


}