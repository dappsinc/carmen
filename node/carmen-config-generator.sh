#!/usr/bin/env bash
#
#   Copyright 2019, Dapps Incorporated.
#
#    Licensed under the Apache License, Version 2.0 (the "License");
#    you may not use this file except in compliance with the License.
#    You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS,
#    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#    See the License for the specific language governing permissions and
#    limitations under the License.
#

# Creates DSOA config
set -e

echo "Creating DSOA config"

# DSOA_COMPATIBILITY_ZONE_URL deprecated. Used to set NETWORKMAP_URL and DOORMAN_URL if set.
DSOA_COMPATIBILITY_ZONE_URL=${DSOA_COMPATIBILITY_ZONE_URL:-https://dsoa.network}

# Corda official environment variables. If set will be used instead of defaults
MY_LEGAL_NAME=${MY_LEGAL_NAME:-O=Dapps-$(od -x /dev/urandom | head -1 | awk '{print $7$8$9}'), OU=Dapps, L=San Mateo, C=US}
MY_PUBLIC_ADDRESS=${MY_PUBLIC_ADDRESS:-localhost}
# MY_P2P_PORT=10200 <- default set in corda dockerfile
NETWORKMAP_URL=${NETWORKMAP_URL:-$DSOA_COMPATIBILITY_ZONE_URL}
DOORMAN_URL=${DOORMAN_URL:-$DSOA_COMPATIBILITY_ZONE_URL}
TRUST_STORE_NAME=${TRUST_STORE_NAME:-truststore.jks}
NETWORK_TRUST_PASSWORD=${NETWORK_TRUST_PASSWORD:-trustpass}
MY_EMAIL_ADDRESS=${MY_EMAIL_ADDRESS:-noreply@dapps-inc.com}
# RPC_PASSWORD=$(cat /dev/urandom | tr -dc 'a-zA-Z0-9' | fold -w 32 | head -n 1) <- not used
# MY_RPC_PORT=10201 <- default set in corda dockerfile.
# MY_RPC_ADMIN_PORT=10202 <- default set in corda dockerfile.
TLS_CERT_CRL_DIST_POINT=${TLS_CERT_CRL_DIST_POINT:-NULL}
TLS_CERT_CERL_ISSUER=${TLS_CERT_CERL_ISSUER:-NULL}


# Cordite environment variables. Will override Corda official environment variables if passed.
DSOA_LEGAL_NAME=${DSOA_LEGAL_NAME:-$MY_LEGAL_NAME}
DSOA_P2P_ADDRESS=${DSOA_P2P_ADDRESS:-$MY_PUBLIC_ADDRESS:$MY_P2P_PORT}
DSOA_KEY_STORE_PASSWORD=${DSOA_KEY_STORE_PASSWORD:-cordacadevpass}
DSOA_TRUST_STORE_PASSWORD=${DSOA_TRUST_STORE_PASSWORD:-$NETWORK_TRUST_PASSWORD}
DSOA_DB_USER=${DSOA_DB_USER:-sa}
DSOA_DB_PASS=${DSOA_DB_PASS:-dbpass}
DSOA_DB_DRIVER=${DSOA_DB_DRIVER:-org.h2.jdbcx.JdbcDataSource}
DSOA_DB_DIR=${DSOA_DB_DIR:-$PERSISTENCE_FOLDER}
DSOA_DB_MAX_POOL_SIZE=${DSOA_DB_MAX_POOL_SIZE:-10}
DSOA_BRAID_PORT=${DSOA_BRAID_PORT:-8080}
DSOA_DEV_MODE=${DSOA_DEV_MODE:-true}
DSOA_DETECT_IP=${DSOA_DETECT_IP:-false}
DSOA_CACHE_NODEINFO=${DSOA_CACHE_NODEINFO:-false}
DSOA_LOG_MODE=${DSOA_LOG_MODE:-normal}
DSOA_JVM_MX=${DSOA_JVM_MX:-1536m}
DSOA_JVM_MS=${DSOA_JVM_MS:-512m}
DSOA_H2_PORT=${DSOA_H2_PORT:-9090}

#set DSOA_DB_URL
h2_db_url="\"jdbc:h2:file:${DSOA_DB_DIR};DB_CLOSE_ON_EXIT=FALSE;LOCK_TIMEOUT=10000;WRITE_DELAY=100;AUTO_SERVER_PORT=${DSOA_H2_PORT}\""
DSOA_DB_URL=${DSOA_DB_URL:-$h2_db_url}

# DSOA_LOG_CONFIG_FILE:
if [ "${DSOA_LOG_MODE}" == "json" ]; then
    DSOA_LOG_CONFIG_FILE=dsoa-log4j2-json.xml
else
    DSOA_LOG_CONFIG_FILE=dsoa-log4j2.xml
fi

# Create node.conf and default if variables not set
echo
echo
printenv
echo
echo
basedir=\"\${baseDirectory}\"
braidhost=${DSOA_LEGAL_NAME#*O=} && braidhost=${braidhost%%,*} && braidhost=$(echo $braidhost | sed 's/ //g')
cat > ${CONFIG_FOLDER}/node.conf <<EOL
myLegalName : "${DSOA_LEGAL_NAME}"
p2pAddress : "${DSOA_P2P_ADDRESS}"

networkServices : {
    "doormanURL" : "${DOORMAN_URL}"
    "networkMapURL" : "${NETWORKMAP_URL}"
}

tlsCertCrlDistPoint : "${TLS_CERT_CRL_DIST_POINT}"
tlsCertCrlIssuer : "${TLS_CERT_CERL_ISSUER}"

dataSourceProperties : {
    "dataSourceClassName" : "${DSOA_DB_DRIVER}"
    "dataSource.url" : "${DSOA_DB_URL}"
    "dataSource.user" : "${DSOA_DB_USER}"
    "dataSource.password" : "${DSOA_DB_PASS}"
    "maximumPoolSize" : "${DSOA_DB_MAX_POOL_SIZE}"
}

keyStorePassword : "${DSOA_KEY_STORE_PASSWORD}"
trustStorePassword : "${DSOA_TRUST_STORE_PASSWORD}"
detectPublicIp : ${DSOA_DETECT_IP}
devMode : ${DSOA_DEV_MODE}
jvmArgs : [ "-Dbraid.${braidhost}.port=${DSOA_BRAID_PORT}", "-Xms${DSOA_JVM_MS}", "-Xmx${DSOA_JVM_MX}", "-Dlog4j.configurationFile=${DSOA_LOG_CONFIG_FILE}" ]
jarDirs=[
    ${basedir}/libs
]
emailAddress : "${MY_EMAIL_ADDRESS}"
EOL



# Configure notaries
# for the moment we're dealing with two systems - later we can do this in a slightly different way
if [ "$DSOA_NOTARY" == "true" ] || [ "$DSOA_NOTARY" == "validating" ] || [ "$DSOA_NOTARY" == "non-validating" ] ; then
    NOTARY_VAL=false
    if [ "$DSOA_NOTARY" == "true" ] || [ "$DSOA_NOTARY" == "validating" ]; then
    NOTARY_VAL=true
    fi
    echo "DSOA_NOTARY set to ${DSOA_NOTARY}. Configuring node to be a notary with validating ${NOTARY_VAL}"
cat >> ${CONFIG_FOLDER}/node.conf <<EOL
notary {
    validating=${NOTARY_VAL}
}
EOL
fi

# do we want to turn on jolokia for monitoring?
if [ ! -z "$DSOA_EXPORT_JMX" ]; then
cat >> ${CONFIG_FOLDER}/node.conf <<EOL
exportJMXTo: "${DSOA_EXPORT_JMX}"
EOL
fi


echo "${CONFIG_FOLDER}/node.conf created:"
cat ${CONFIG_FOLDER}/node.conf

if [ ! -z "$DSOA_METERING_CONFIG" ] ; then
   echo "DSOA_METERING_CONFIG set to ${DSOA_METERING_CONFIG}. Creating metering-service-config.json"
   echo $DSOA_METERING_CONFIG > metering-service-config.json
fi

if [ ! -z "$DSOA_FEE_DISPERSAL_CONFIG" ] ; then
   echo "DSOA_FEE_DISPERSAL_CONFIG set to ${DSOA_FEE_DISPERSAL_CONFIG}. Creating fee-dispersal-service-config.json"
   echo $DSOA_FEE_DISPERSAL_CONFIG > fee-dispersal-service-config.json
fi
